import pandas as pd
import numpy as np
from pickle import NONE
import copy
import time
import datetime
import package_hc.tool_data_oracle as td_oracle

# 将df转换成列表的函数
def datelist(df):
    enddatelist = df.values.tolist()
    enddatelist = [ x[0] for x in enddatelist]
    return enddatelist

# 定义查询数据库地址
#dbaddress2='finchina/finchina@192.168.1.8:1521/DSPDB'
dbaddress1='HCQ/hcq@192.168.1.8/dspdb'

# 连接存储数据库，看要更新哪些日期的数据
oracle1 = td_oracle.OracleDb(dbaddress1) 
# 查看要计算日期列表
df_enddatelist = oracle1.execute_querydf('select distinct(enddate) from HC_FA_FUND_FACTOR')
enddatelist = df_enddatelist.values.tolist()
enddatelist = [ x[0] for x in enddatelist]

# 查询完，关闭储存数据库
oracle1.cursor.close()
oracle1.conn.close()


# 拆分列表
def list_of_groups(list_info, per_list_len):
    '''
    :param list_info:   列表
    :param per_list_len:  每个小列表的长度
    :return:
    '''
    list_of_group = zip(*(iter(list_info),) *per_list_len) 
    end_list = [list(i) for i in list_of_group] # i is a tuple
    count = len(list_info) % per_list_len
    end_list.append(list_info[-count:]) if count !=0 else end_list
    return end_list

datalist_df=pd.DataFrame(list_of_groups(enddatelist,4))

def factorbuild_fundstyle_return_sub(secodelist=None,enddate=None,cyear=1):
    '''
    input:
    测试数据 
    type:strexcel 

    output: 
    [[基金内码，更新日期，因子名称，因子数值]，
    [基金内码，更新日期，因子名称，因子数值]，
    ...
    [基金内码，更新日期，因子名称，因子数值]   ]
    type: list
    ''' 
    #因子结果存储结构
    df_factor=pd.DataFrame(columns=['SECODE','TRADEDAY','FACTORCODE','FACTORVALUE'])

    oracle =td_oracle.OracleDb(dbaddress) 
    #算3年期年化收益率
    startdatelist3 =  [(datetime.datetime.strptime(enddate,'%Y%m%d') - datetime.timedelta(days = a + int(365*cyear) )).strftime('%Y%m%d')  for a in range(10)][::-1]
    enddatelist = [(datetime.datetime.strptime(enddate,'%Y%m%d') - datetime.timedelta(days = a )).strftime('%Y%m%d')  for a in range(10)][::-1]



    sqlstr_1='''
    SELECT c.SECODE,c.ENDDATE ENDDATE_start, c.REPAIRUNITNAV NV_start
    FROM TQ_FD_DERIVEDN c
    WHERE  c.ENDDATE IN ({})
    '''.format( 'UNION ALL'.join([" SELECT '{}' FROM dual "]*len(startdatelist3)).format(*startdatelist3)  )


    df_1=oracle.execute_querydf(sqlstr_1) 
    if len(df_1)==0:
        return df_factor
    df_1=df_1.loc[df_1.SECODE.isin(secodelist),:]
    df_1=df_1.sort_values(['SECODE','ENDDATE_START'])
    df_1=df_1.drop_duplicates(subset=['SECODE'], keep='last')

    sqlstr_2='''
    SELECT c.SECODE,c.ENDDATE, c.REPAIRUNITNAV NV_end
    FROM TQ_FD_DERIVEDN c
    WHERE  c.ENDDATE IN ({})
    '''.format( 'UNION ALL'.join([" SELECT '{}' FROM dual "]*len(enddatelist)).format(*enddatelist)  )


    df_2=oracle.execute_querydf(sqlstr_2) 
    if len(df_2)==0:
        return df_factor
    df_2=df_2.loc[df_2.SECODE.isin(secodelist),:]

    df_2=df_2.sort_values(['SECODE','ENDDATE'])
    df_2=df_2.drop_duplicates(subset=['SECODE'], keep='last')

    df_cmb=pd.merge(df_2,df_1,on=['SECODE'],how='left')
    df_cmb=df_cmb.dropna(subset=['NV_START'])#删除起始日期没有数据的
    if len(df_cmb)==0:
        return df_factor
    df_cmb['days']=df_cmb.apply(lambda x:(datetime.datetime.strptime(x['ENDDATE'],'%Y%m%d')-datetime.datetime.strptime(x['ENDDATE_START'],'%Y%m%d')).days ,axis=1)#交易日期间隔
    df_cmb['ret_year']=(df_cmb['NV_END']/df_cmb['NV_START'])**(365/df_cmb['days'])-1
    df_factor['SECODE']=df_cmb.SECODE.tolist()
    df_factor['TRADEDAY']=df_cmb.ENDDATE.tolist()
    df_factor['FACTORCODE']='ret_year'+str(cyear)
    df_factor['FACTORVALUE']=df_cmb.ret_year.tolist()
    return df_factor


def factorbuild_fundstyle_return(secodelist=None,enddate=None):
    '''

    input:
    测试数据 
    type:strexcel 

    output: 
    [[基金内码，更新日期，因子名称，因子数值]，
    [基金内码，更新日期，因子名称，因子数值]，
    ...
    [基金内码，更新日期，因子名称，因子数值]   ]
    type: list

    ''' 
    # dbaddress='finchina/finchina@10.119.10.2:1521/jydb'
    #因子结果存储结构
    df_factor=pd.DataFrame(columns=['SECODE','TRADEDAY','FACTORCODE','FACTORVALUE'])
    #算3年期年化收益率
    print('计算3年期年化收益率')
    df_factor_3=factorbuild_fundstyle_return_sub(secodelist=secodelist,enddate=enddate,cyear=3)
    df_factor=df_factor.append(df_factor_3)
    print('3年期年化收益率计算完成，共有数据行数--'+str(len(df_factor_3)))
    #算2年期年化收益率
    print('计算2年期年化收益率')
    df_factor_2=factorbuild_fundstyle_return_sub(secodelist=secodelist,enddate=enddate,cyear=2)
    df_factor=df_factor.append(df_factor_2)
    print('2年期年化收益率计算完成，共有数据行数--'+str(len(df_factor_2)))

    #算1年期年化收益率
    print('计算1年期年化收益率')
    df_factor_1=factorbuild_fundstyle_return_sub(secodelist=secodelist,enddate=enddate,cyear=1)
    df_factor=df_factor.append(df_factor_1)
    print('1年期年化收益率计算完成，共有数据行数--'+str(len(df_factor_2)))


    return df_factor


for i in enddatelist:
    enddate=i
    dbaddress1='HCQ/hcq@192.168.1.8/dspdb'
    oracle1 = td_oracle.OracleDb(dbaddress1) 
    # 得到这个计算日期要计算的基金内码的列表
    eachenddate_secode_sql = 'select distinct(secode) from HC_FA_FUND_FACTOR where enddate={}'.format(enddate)
    eachenddate_secode_list = datelist(oracle1.execute_querydf(eachenddate_secode_sql))
    # 查询完，关闭储存数据库
    oracle1.cursor.close()
    oracle1.conn.close()
    
    secodelist=eachenddate_secode_list
    
    dbaddress='finchina/finchina@192.168.1.8:1521/DSPDB'
    
    #根据不同的参数计算收益率
   
    print('*****************计算ret_year1/2/3(完整版)*****************')
    path='{}_fundstyle_return.csv'.format(enddate)
    df_factor=factorbuild_fundstyle_return(secodelist=secodelist,enddate=enddate)
    print('计算完毕，数据保存到本地--' + path)
    df_factor.to_csv(path_or_buf=path,index=False, encoding='utf_8_sig')


    
    
    
